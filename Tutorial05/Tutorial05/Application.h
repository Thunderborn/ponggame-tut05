#pragma once
#include "Gameplay.h"
#include "SFML/Graphics.hpp"
#include <SFML/Audio.hpp>

class Application
{
public:
	Application();
	void Run();
	//global way to see how much time passed since the last update
	static float GetElapsedSecs() { return sElapsedSecs.asSeconds(); }
private:
	
	static sf::Time sElapsedSecs;	//track how much time each update/render takes for smooth motion


};




