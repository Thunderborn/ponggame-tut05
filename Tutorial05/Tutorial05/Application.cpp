#include "Application.h"



sf::Time Application::sElapsedSecs;	//as it's static it need instantiating separately

Application::Application() {}


void Application::Run()
{
	Gameplay gameplay;
	sf::RenderWindow window (sf::VideoMode(GDC::SCREEN_RES.x, GDC::SCREEN_RES.y), "Pong Game");
	sf::Clock clock;
	gameplay.Initialise(window);
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();


			}
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode < GDC::ASCII_RANGE)
					gameplay.TextEntered(static_cast<char>(event.text.unicode));


			}


		}

		sElapsedSecs = clock.restart();

		window.clear();

		
		gameplay.Update(window);
		gameplay.Render(window);


		window.display();


	}

}