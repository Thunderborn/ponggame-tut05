#include "GameObject.h"
#include "Gameplay.h"



void::GameObject::Initialise(sf::RenderWindow& window)
{
	//setting initial position of the game objects
	player1.setPosition(GDC::PLAYER_1_X_POS, GDC::PLAYER_1_Y_POS);
	player2.setPosition(GDC::PLAYER_2_X_POS, GDC::PLAYER_2_Y_POS);


	//load the font
	if (!font.loadFromFile("data/font.ttf"))
		assert(false);

	//load sound effects
	if (!bounce1.loadFromFile("data/bounce1.wav"))
		assert(false);

	if (!bounce2.loadFromFile("data/bounce2.wav"))
		assert(false);

	if (!scoresound.loadFromFile("data/score.wav"))
		assert(false);

	if (!victory.loadFromFile("data/victory.wav"))
		assert(false);

	if (!music.openFromFile("data/music.wav"))
		assert(false);
	music.play();

	//load textures
	if (!texcourt.loadFromFile("data/court.jpg"))
		assert(false);
	texcourt.setSmooth(true);

	if (!texball.loadFromFile("data/ball.png"))
		assert(false);
	texball.setSmooth(true);

	if (!texred.loadFromFile("data/red.jpg"))
		assert(false);
	texred.setSmooth(true);

	if (!texgreen.loadFromFile("data/green.jpeg"))
		assert(false);
	texgreen.setSmooth(true);

}


void GameObject::RenderObjects(sf::RenderWindow& window)
{
	sf::Sprite courtsprite(texcourt);
	courtsprite.setOrigin(texcourt.getSize().x / 2.f, texcourt.getSize().y / 2.f);
	courtsprite.setPosition(GDC::SCREEN_RES.x / 2.f, GDC::SCREEN_RES.y / 2.f);
	courtsprite.setScale(0.4, 0.4);
	courtsprite.setRotation(90);
	window.draw(courtsprite);

	player1.setTexture(&texgreen);
	player1.setOrigin(player1.getSize().x / 2.f, player1.getSize().y / 2.f);
	player1.setSize(sf::Vector2f(GDC::gridSize, 5.f * (GDC::gridSize)));
	window.draw(player1);

	player2.setTexture(&texred);
	player2.setOrigin(player2.getSize().x / 2.f, player2.getSize().y / 2.f);
	player2.setSize(sf::Vector2f(GDC::gridSize, 5.f * (GDC::gridSize)));
	window.draw(player2);

	ball.setTexture(&texball);
	ball.setOrigin(GDC::BALL_SIZE, GDC::BALL_SIZE);
	ball.setRadius(GDC::BALL_SIZE);
	window.draw(ball);

	


}


void GameObject::CollisionChecking()
{
	if (player1.getPosition().y < 50.f)
	{
		player1.setPosition(GDC::PLAYER_1_X_POS, 50.f);
	}

	if (player1.getPosition().y > GDC::SCREEN_RES.y - 50.f)
	{
		player1.setPosition(GDC::PLAYER_1_X_POS, GDC::SCREEN_RES.y - 50.f);
	}

	if (player2.getPosition().y < 50.f)
	{
		player2.setPosition(GDC::PLAYER_2_X_POS, 50.f);
	}

	if (player2.getPosition().y > GDC::SCREEN_RES.y - 50.f)
	{
		player2.setPosition(GDC::PLAYER_2_X_POS, GDC::SCREEN_RES.y - 50.f);
	}


}


void GameObject::BallMovement()
{
	ball_velocity.x += ball_speed_x * Application::GetElapsedSecs();
	ball_velocity.y += ball_speed_y * Application::GetElapsedSecs();
	ball.move(ball_velocity);


	//behaviour of the ball
	if (ball.getPosition().x > player2.getPosition().x - GDC::gridSize && ball.getPosition().y > player2.getPosition().y - 50.f && ball.getPosition().y < player2.getPosition().y + 50.f)
	{
		ball_speed_x = -1.2f * ball_speed_x;
		sound.setBuffer(bounce1);
		sound.play();
		if (ball_speed_x > 570.f)
			ball_speed_x = 573.f;
	}

	else if (ball.getPosition().x < player1.getPosition().x + GDC::gridSize && ball.getPosition().y > player1.getPosition().y - 50.f && ball.getPosition().y < player1.getPosition().y + 50.f)
	{
		ball_speed_x = -1.2f * ball_speed_x;
		sound.setBuffer(bounce2);
		sound.play();
		if (ball_speed_x > 570.f)
			ball_speed_x = 573.f;
	}

	if (ball.getPosition().y > GDC::SCREEN_RES.y - GDC::BALL_SIZE)
	{
		ball_speed_y = -ball_speed_y;
	}

	else if (ball.getPosition().y < GDC::BALL_SIZE)
	{
		ball_speed_y = -ball_speed_y;
	}



}


void GameObject::SetBallPosition()
{
	if (unlocked == false)
	{
		if (counter == 0 || counter == 2 || counter == 4 || counter == 6 || counter == 8 || counter == 10)
		{
			ball.setPosition(player1.getPosition().x + GDC::gridSize * 2, player1.getPosition().y);
			ball_speed_x = 160.f;
			ball_speed_y = 160.f;
		}
		else
		{
			ball.setPosition(player2.getPosition().x - GDC::gridSize * 2, player2.getPosition().y);
			ball_speed_x = -160.f;
			ball_speed_y = 160.f;
		}
	}
}