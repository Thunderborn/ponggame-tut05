#include "Gameplay.h"
using namespace std;


Gameplay::Gameplay() {}


void Gameplay::Initialise(sf::RenderWindow& window)
{
	GameObject::Initialise(window);


}


void Gameplay::Update(sf::RenderWindow& window)
{
	SetBallPosition();
	velocity1.y = 0.f;
	velocity2.y = 0.f;
	ball_velocity.x = 0.f;
	ball_velocity.y = 0.f;
	Gameplay::BallMovement();
	Gameplay::InputProcessing();
	Gameplay::CollisionChecking();
	Gameplay::Score();
	
}


void Gameplay::Render(sf::RenderWindow& window)
{
	GameObject::RenderObjects(window);
	Gameplay::ScoreDisplaying(window);
	if (((ball.getPosition().x > GDC::SCREEN_RES.x) || (ball.getPosition().x < 0.f)) && counter < 12)
	{
		Gameplay::NextRound(window);
		counter = counter + 1;
	}
	if (counter > 10)
	{
		Gameplay::GameOver(window);
	}
	
	
}


void Gameplay::InputProcessing()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		velocity1.y += -(GDC::MOVEMENT_SPEED)*Application::GetElapsedSecs();
		player1.move(velocity1);
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		velocity1.y += GDC::MOVEMENT_SPEED * Application::GetElapsedSecs();
		player1.move(velocity1);
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		velocity2.y += -(GDC::MOVEMENT_SPEED)*Application::GetElapsedSecs();
		player2.move(velocity2);
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		velocity2.y += GDC::MOVEMENT_SPEED * Application::GetElapsedSecs();
		player2.move(velocity2);
	}

	if (counter < 12)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			SetBallPosition();
			unlocked = true;
		}
	}


}


void Gameplay::Score()
{
	if (counter < 11)
	{
		if (ball.getPosition().x > GDC::SCREEN_RES.x)
		{
			P1_Score = P1_Score + 1;
			sound.setBuffer(scoresound);
			sound.play();
		}

		if (ball.getPosition().x < 0.f)
		{
			P2_Score = P2_Score + 1;
			sound.setBuffer(scoresound);
			sound.play();
		}
	}
	stringstream currentScore_ss;
	currentScore_ss << P1_Score << " VS " << P2_Score;
	currentScore = currentScore_ss.str();

}


void Gameplay::ScoreDisplaying(sf::RenderWindow& window)
{
	//display current score
	score.setFont(font);
	score.setString(currentScore);
	score.setCharacterSize(50);
	score.setFillColor(sf::Color::White);
	score.setPosition(GDC::SCREEN_RES.x/2 - 100.f, GDC::SCREEN_RES.y/2 - 230.f);
	window.draw(score);



}


void Gameplay::NextRound(sf::RenderWindow& window)
{
	unlocked = false;
	player1.setPosition(GDC::PLAYER_1_X_POS, GDC::PLAYER_1_Y_POS);
	player2.setPosition(GDC::PLAYER_2_X_POS, GDC::PLAYER_2_Y_POS);
}


void Gameplay::DisplayWhoWon(sf::RenderWindow& window)
{
	sound.setBuffer(victory);
	sound.play();
	whoWon.setFont(font);
	whoWon.setCharacterSize(35);
	whoWon.setPosition((GDC::SCREEN_RES.x / 2) - 300.f, GDC::SCREEN_RES.y / 2 - 100.f);
	if (P1_Score > P2_Score)
	{
		whoWon.setString("PLAYER 1 WON! Congratulations!");
		window.draw(whoWon);
	}
	else
	{
		whoWon.setString("PLAYER 2 WON! Congratulations!");
		window.draw(whoWon);
	}



}


void Gameplay::GameOver(sf::RenderWindow& window)
{
	unlocked = true;
	player1.setPosition(GDC::SCREEN_RES.x / 2.f - 60.f, GDC::SCREEN_RES.y / 2.f);
	player2.setPosition(GDC::SCREEN_RES.x / 2.f + 60.f, GDC::SCREEN_RES.y / 2.f);
	ball.setPosition(GDC::SCREEN_RES.x / 2, GDC::SCREEN_RES.y / 2);
	ball_speed_x = 0.f;
	ball_speed_y = 0.f;
	score.setCharacterSize(80);
	score.setPosition(GDC::SCREEN_RES.x / 2 - 120.f, GDC::SCREEN_RES.y / 2 - 250.f);
	music.stop();
	DisplayWhoWon(window);
	


}

void Gameplay::TextEntered(char)
{


}