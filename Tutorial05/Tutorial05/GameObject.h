#pragma once
#include"SFML/Graphics.hpp"
#include <SFML/Audio.hpp>
class GameObject
{
public:
	void Initialise(sf::RenderWindow&);
	void CollisionChecking();	//make sure paddles won't go off the screen
	void BallMovement();
	void SetBallPosition();		//ball will follow once first paddle and once second one
	void RenderObjects(sf::RenderWindow&);
	sf::Vector2f velocity1;
	sf::Vector2f velocity2;
	sf::Vector2f ball_velocity;
	sf::RectangleShape player1;
	sf::RectangleShape player2;
	sf::CircleShape ball;
	std::string currentScore;
	sf::Text score;
	sf::Text whoWon;
	sf::Font font;
	sf::Music music;	//background music
	sf::SoundBuffer bounce1;  //paddle hit sound
	sf::SoundBuffer bounce2;  //paddle hit sound
	sf::SoundBuffer scoresound;
	sf::SoundBuffer victory;	//victory sound
	sf::Sound sound;
	sf::Sound sound2;
	sf::Texture texred;
	sf::Texture texgreen;
	sf::Texture texcourt;
	sf::Texture texball;
	int counter = 0;	//round counter
	int P1_Score = 0;
	int P2_Score = 0;
	float ball_speed_x = 0.f; //ball speed x direction
	float ball_speed_y = 0.f; //ball speed y direction
	bool unlocked = false;	//check is the ball should still follow the paddle


};

