#pragma once
#include "SFML/Graphics.hpp"
#include <SFML/Audio.hpp>
#include "Application.h"
#include "GameObject.h"
#include <assert.h>
#include <iostream>
#include <sstream>
#include <string.h>

//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
};


//keeping all constants under one namespace
namespace GDC
{
	const Vec2i SCREEN_RES{ 700,500 };
	const sf::Uint32 ASCII_RANGE{ 127 }; //google an ASCII table, after 127 we don't care
	const float MOVEMENT_SPEED{ 140.f }; //paddle movement speed
	const float gridSize{ 20.f };
	const float PLAYER_1_X_POS{ GDC::SCREEN_RES.x / 2.f - 310.f };
	const float PLAYER_1_Y_POS{ GDC::SCREEN_RES.y / 2.f };
	const float PLAYER_2_X_POS{ GDC::SCREEN_RES.x / 2.f + 310.f };
	const float PLAYER_2_Y_POS{ GDC::SCREEN_RES.y / 2.f };
	const float BALL_SIZE{ 10.f };
}


class Gameplay: GameObject
{
public:
	Gameplay();
	void Initialise(sf::RenderWindow&);
	void Update(sf::RenderWindow&);		//main update function
	void Render(sf::RenderWindow&);		//main render function
	void TextEntered(char);
	void InputProcessing();
	void Score();
	void ScoreDisplaying(sf::RenderWindow&);
	void NextRound(sf::RenderWindow&);	//preparing objects for next round
	void DisplayWhoWon(sf::RenderWindow&);
	void GameOver(sf::RenderWindow&);	//game over with the score
	

	


};
